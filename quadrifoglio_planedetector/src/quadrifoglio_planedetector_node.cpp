#include <vector>
#include <algorithm>
#include "ros/ros.h"
#include <pcl_ros/point_cloud.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include "visualization_msgs/Marker.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/PointCloud2.h"

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <Eigen/Dense>

#define TODEG 57.29577951 //Conversion factors for degrees & radians
#define TORAD 0.017453293 //The AVR cannot reach this level of precision but w/e


visualization_msgs::Marker Sphere(float x, float y, int r, int g, int b, float size, int id){
        visualization_msgs::Marker marker;
        marker.header.frame_id = "laser";
        marker.header.stamp = ros::Time();
        marker.ns = "my_namespace";
        marker.id = id;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = x;
        marker.pose.position.y = y;
        marker.pose.position.z = 0;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;
        marker.scale.x = size;
        marker.scale.y = size;
        marker.scale.z = size;
        marker.color.a = 1.0; // Don't forget to set the alpha!
        marker.color.r = r;
        marker.color.g = g;
        marker.color.b = b;
        return marker;
}
visualization_msgs::Marker Flat(float x, float y, float rot, int r, int g, int b, float size, int id){
        visualization_msgs::Marker marker;
        marker.header.frame_id = "laser";
        marker.header.stamp = ros::Time();
        marker.ns = "planes";
        marker.id = id;
        marker.type = visualization_msgs::Marker::CUBE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.pose.position.x = x;
        marker.pose.position.y = y;
        marker.pose.position.z = 0;
        tf2::Quaternion myQuaternion;
        myQuaternion.setRPY( 0, 0, rot );
        marker.pose.orientation = tf2::toMsg(myQuaternion);
        marker.scale.x = 0.01;
        marker.scale.y = size;
        marker.scale.z = 0.1;
        marker.color.a = 0.4; // Don't forget to set the alpha!
        marker.color.r = r;
        marker.color.g = g;
        marker.color.b = b;
        marker.lifetime = ros::Duration(0.2);
        return marker;
}
visualization_msgs::Marker Line(float x, float y, float x2, float y2, int id){
        visualization_msgs::Marker marker;
        marker.header.frame_id = "laser";
        marker.header.stamp = ros::Time();
        marker.ns = "my_namespace";
        marker.id = id;
        marker.type = visualization_msgs::Marker::LINE_STRIP;
        marker.action = visualization_msgs::Marker::ADD;
        marker.scale.x = marker.scale.y = marker.scale.z = 0.03;
        marker.color.a = 1.0; // Don't forget to set the alpha!
        marker.color.r = 0.0;
        marker.color.g = 0.0;
        marker.color.b = 1.0;

        geometry_msgs::Point p1;
        geometry_msgs::Point p2;
        p1.x = x;
        p1.y = y;
        p1.z = 0;
        p2.x = x2;
        p2.y = y2;
        p2.z = 0;
        marker.points.push_back(p1);
        marker.points.push_back(p2);
        return marker;
}

class Virtualplanedetector
{

  public:
    Virtualplanedetector(ros::Publisher *debugCloudPub, ros::Publisher *visPub, image_transport::Publisher *imagePub, int minVotes, float maxRange, float angleStep, float distanceStep)
    {
        _debugCloudPub = debugCloudPub;
        _visPub = visPub;
        _imagePub = imagePub;

        _angleQuant = (unsigned int)(180.0/angleStep);
        _distQuant = (unsigned int)(maxRange/distanceStep);
        _v={_angleQuant, std::vector<int>(_distQuant)};
        _maxdist = maxRange;
        _anglestep = angleStep;
        _diststep = distanceStep;
        _minvotes = minVotes;
        //cv::Mat accumulatorImage(1544, 2064, CV_8UC3, cv::Scalar(20,20,20));
        accumulatorImage = {(int)_distQuant, (int)_angleQuant, CV_8UC3, cv::Scalar(20,20,20)};
        ROS_INFO("Angle steps: %d Distance steps: %d ",_angleQuant, _distQuant);
    }

    void pclCallback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &pclMsg)
    {
        //_pcl = *pclMsg;

        _debugCloudMsg.header = pclMsg->header;
        _debugCloudMsg.points.clear();
        long sizeCounter = 0;
        int frontBinHits = 0;
        int rearBinHits = 0;
        float lowest = 999;
        float highest = -999;

        float closest = 7777;
        float closestx = 0;
        float closesty = 0;

        for(int angles = 0;angles < _angleQuant ; angles++){
            for(int distances = 0; distances < _distQuant ; distances++){
                _v[angles][distances] = 0;  //Zero all distance bins for all angles
            }
        }

        for (auto point : pclMsg->points)
        { //negative x is forward, negative y is left wrt rplidar

            float distFromOrigo = sqrt(point.x*point.x + point.y*point.y);
            if(distFromOrigo < closest){
                closest = distFromOrigo;
                closestx = point.x;
                closesty = point.y;
            }
            for(float i = 0; i < _angleQuant; i++){  //Go through all angles
                float supportline = point.x*cos(TORAD*_anglestep*i)+point.y*sin(TORAD*_anglestep*i); //distance of line from origo
                //float result = ((supportline + 5.0/2.0) / 5.0) * multiple;
                int supportBin = supportline / _diststep;  //Divide into bins of 5 cm
                //supportBin = std::min(std::max(0,supportBin),_distQuant);
                if(supportBin >= 0 && supportBin < _distQuant){ //If in range, put vote in range bin
                    _v[i][supportBin] ++;
                }
            }
            
        }
        //Find bin with most votes
/*         int mostAngle = -1;
        int mostSupport = -1;
        int mostest = 0;
        for(int angles = 0;angles < _angleQuant ; angles++){
            for(int distances = 0; distances < _distQuant ; distances++){
                int votes = _v[angles][distances];
                if(votes > mostest){
                    mostAngle = angles;
                    mostSupport = distances;
                    mostest = votes;
                }
            }
        } */
        int lineCount = 0;
        int vizId = 10;
        for(int angles = 0;angles < _angleQuant ; angles++){
            for(int distances = 0; distances < _distQuant ; distances++){
                int votes = _v[angles][distances];
                //ROS_INFO("trying: %d", (int)_distQuant-distances));
                accumulatorImage.at<cv::Vec3b>(cv::Point(angles,((int)_distQuant)-distances-1)) = cv::Vec3b(votes*4,votes*2,votes*2); //visualize accumulator
                //accumulatorImage.at<cv::Vec3b>(cv::Point(angles,distances)) = cv::Vec3b(angles,distances,0);
                if(votes > _minvotes){
                    lineCount++;
                    float actualLen = distances*_diststep;
                    float X = actualLen*cos(TORAD*_anglestep*angles);
                    float Y = actualLen*sin(TORAD*_anglestep*angles);
                    _visPub->publish(Flat(X, Y, TORAD*_anglestep*angles,1,1,0,1,vizId));
                    vizId++;
                }
            }
        }


       // ROS_INFO("Most votes (%d) for angle: %d distance: %d",mostest, mostAngle, mostSupport);
        ROS_INFO("Lines found: %d",lineCount);
        //ROS_INFO("Front planedetector blocked: %d Rear planedetector blocked: %d", _frontBlocked, _rearBlocked);
        _debugCloudMsg.width = sizeCounter;
        _debugCloudMsg.height = 1;
        _debugCloudPub->publish(_debugCloudMsg);
        //float actualLen = mostSupport*0.05;

        _visPub->publish(Sphere(0, 0, 1,0,0,0.1,0));  //Red point in origo
        _visPub->publish(Sphere(closestx, closesty, 0,1,0,0.1,1));
        //_visPub->publish(Flat(0, 0, TORAD*mostAngle,1,1,0,1,3));

/*         float closestX = actualLen*cos((float)mostAngle*TORAD);
        float closestY = actualLen*sin((float)mostAngle*TORAD);
        float drawX1 = closestX - 1.0*sin((float)mostAngle*TORAD);
        float drawY1 = closestY + 1.0*cos((float)mostAngle*TORAD);
        float drawX2 = closestX + 1.0*sin((float)mostAngle*TORAD);
        float drawY2 = closestY - 1.0*cos((float)mostAngle*TORAD); */

        //_visPub->publish(Line(drawX1, drawY1, drawX2,drawY2, 2));
        _imagePub->publish(cv_bridge::CvImage(std_msgs::Header(), "bgr8", accumulatorImage).toImageMsg());
    }

  private:
    ros::Publisher *_debugCloudPub;
    ros::Publisher *_visPub;
    image_transport::Publisher *_imagePub;
    pcl::PointCloud<pcl::PointXYZ> _debugCloudMsg;
    unsigned int _angleQuant;  //How many discrete steps in the 180-degree search area
    unsigned int _distQuant; //How many discrete steps within the range
    //std::vector<std::vector<int>> _v{_angleQuant, std::vector<int>(_distQuant)}; //2-dimensional vector, distances per angles
    std::vector<std::vector<int>> _v;
    //std::vector<std::vector<int>> fog{10, std::vector<int>(20)};
    float _maxdist;
    float _anglestep;
    float _diststep;
    int _minvotes;
    cv::Mat accumulatorImage;  //This will be filled with a representation of the hough transform accumulator
};

int main(int argc, char **argv)
{

    ros::init(argc, argv, "quadrifoglio_planedetector");
    ros::NodeHandle n;
    ros::NodeHandle n_priv("~");

    float maxRange = 12.0;
    float angleStep = 1.0;
    float distanceStep = 0.05;
    int minVotes = 80;

    n_priv.param<float>("maxrange", maxRange, 12.0);
    n_priv.param<float>("anglestep", angleStep, 1.0);
    n_priv.param<float>("distancestep", distanceStep, 0.05);
    n_priv.param<int>("minvotes", minVotes, 80);

    ROS_INFO("Maxrange parameter: %f", maxRange);

    ros::Publisher cmdVelOutPub = n.advertise<geometry_msgs::Twist>("cmd_vel", 1);
    ros::Publisher debugCloudPub = n.advertise<pcl::PointCloud<pcl::PointXYZ>>("debug_cloud", 1);
    ros::Publisher vis_pub = n.advertise<visualization_msgs::Marker>( "visualization_marker", 0 );
    geometry_msgs::Twist cmdVelOutMsg;

    //For accumulator visualization
    image_transport::ImageTransport it(n);
    image_transport::Publisher imagePub;
    imagePub = it.advertise("accumulator_image",1);

    Virtualplanedetector virtualplanedetector(&debugCloudPub, &vis_pub, &imagePub, minVotes, maxRange,angleStep,distanceStep);

    ros::Subscriber pclSub = n.subscribe("pointcloud", 1, &Virtualplanedetector::pclCallback, &virtualplanedetector);

    ros::spin();

    return 0;
}
